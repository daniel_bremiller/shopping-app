import {EventEmitter, Injectable} from '@angular/core';
import {Recipe} from "./recipe";
import {Ingrediant} from "../shared/ingrediant";

@Injectable()
export class RecipeService {
  private recipes: Recipe[] = [
    new Recipe('testRecipe1', 'simple1', 'http://www.seriouseats.com/images/2015/09/20150914-pressure-cooker-recipes-roundup-09.jpg',[
      new Ingrediant('apples', 5), new Ingrediant('oranges', 2)
    ]),
    new Recipe('testRecipe2', 'simple2', 'http://www.seriouseats.com/images/2015/09/20150914-pressure-cooker-recipes-roundup-09.jpg',[
      new Ingrediant('apples', 5), new Ingrediant('oranges', 2)
      ]),
    new Recipe('testRecipe3', 'simple3', 'http://www.seriouseats.com/images/2015/09/20150914-pressure-cooker-recipes-roundup-09.jpg', [
      new Ingrediant('apples', 5), new Ingrediant('oranges', 2)
    ])
  ];
  recipeSelected: EventEmitter<Recipe> = new EventEmitter();
  constructor() { }

  getRecipes() {
    return this.recipes.slice();
  }
}
