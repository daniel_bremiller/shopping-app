import {EventEmitter, Injectable} from '@angular/core';
import {Ingrediant} from '../shared/ingrediant';

@Injectable()
export class ShoppingListService {

  private ingredients: Ingrediant[] = [
    new Ingrediant('apples', 5), new Ingrediant('oranges', 2)
  ];

  ingredientsChanged: EventEmitter<Ingrediant[]>= new EventEmitter();
   constructor() { }

  getIngredients() {
     return this.ingredients.slice();
  }

  addIngredient(ingrediant: Ingrediant) {
    this.ingredients.push(ingrediant);
    this.ingredientsChanged.emit(this.ingredients.slice());
  }

  addIngredients(ingredients: Ingrediant[]) {
    this.ingredients.push(...ingredients);
    this.ingredientsChanged.emit(this.ingredients.slice());
   }
}
