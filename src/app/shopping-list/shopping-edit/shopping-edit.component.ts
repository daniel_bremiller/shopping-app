import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Ingrediant} from "../../shared/ingrediant";
import {ShoppingListService} from "../shopping-list.service";

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
  }
  onAdd(name: string, amount: string) {
    this.shoppingListService.addIngredient(new Ingrediant(name, Number.parseInt(amount)));
  }

}
