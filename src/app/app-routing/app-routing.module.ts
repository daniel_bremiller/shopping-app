import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PageNotFoundComponent} from '../page-not-found/page-not-found.component';
import {ShoppingListComponent} from '../shopping-list/shopping-list.component';
import {RecipeComponent} from '../recipe/recipe.component';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  {path: '', redirectTo: '/recipes', pathMatch: 'full'},
  {path: 'recipes', component: RecipeComponent},
  {path: 'shopping-list', component: ShoppingListComponent},
  {path: 'page-not-found', component: PageNotFoundComponent},
  {path: '**', redirectTo: '/page-not-found'}

]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [ RouterModule],
  declarations: []
})
export class AppRoutingModule { }
