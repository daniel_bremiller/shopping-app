import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  navLinks = [{'label': 'Recipe Book', 'link' : '/recipes'}, {'label': 'Shopping List', 'link' : '/shopping-list'}];

  constructor() { }

  ngOnInit() {
  }


}
